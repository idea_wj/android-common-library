package com.wj.demo.ui.activity;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.wj.demo.R;
import com.wj.demo.ui.base.BaseActivity;
import com.wj.library.helper.ToastHelper;
import com.wj.library.helper.ToolbarHelper;
import com.wj.library.listener.MyOnClickListener;
import com.wj.library.widget.EmptyLayout;

import butterknife.BindView;

/**
 * EmptyLayout的使用
 * Created by wuj on 2016/6/26.
 * @version 1.0
 */
public class DemoEmptyLayoutActivity extends BaseActivity {
    private static String TAG = DemoEmptyLayoutActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.bt_empty)
    Button btEmpty;

    @BindView(R.id.bt_loading)
    Button btLoading;

    @BindView(R.id.bt_reset)
    Button btReset;

    @BindView(R.id.ll_empty)
    EmptyLayout llEmptyLayout;

    @Override
    protected int getLayout() {
        return R.layout.activity_demo_empty_layout;
    }

    @Override
    protected void initView() {
        llEmptyLayout.setOnClickListener(new MyOnClickListener() {
            @Override
            public void myOnClick(View view) {
                ToastHelper.getInstance().toastShort(DemoEmptyLayoutActivity.this,"重新加载成功!");
                llEmptyLayout.setImg(R.mipmap.demo_emptylayout_empty);
                llEmptyLayout.setType(EmptyLayout.NODATA,false);
            }
        });

        tvTitle.setText("EmptyLayout");
        btEmpty.setOnClickListener(this);
        btLoading.setOnClickListener(this);
        btReset.setOnClickListener(this);
        ToolbarHelper.getInstance().initToolbar(this,toolbar,R.mipmap.ic_back);
    }

    @Override
    protected void myOnClick(View view) {
        super.myOnClick(view);
        switch(view.getId()){
            case R.id.bt_empty:
                llEmptyLayout.setImg(R.mipmap.demo_emptylayout_empty);
                llEmptyLayout.setType(EmptyLayout.NODATA,false);
                break;
            case R.id.bt_loading:
                llEmptyLayout.setType(EmptyLayout.LOADING,false);
                break;
            case R.id.bt_reset:
                llEmptyLayout.setImg(R.mipmap.demo_empty_error);
                llEmptyLayout.setType(EmptyLayout.ERROR,true);
                break;
        }
    }
}
