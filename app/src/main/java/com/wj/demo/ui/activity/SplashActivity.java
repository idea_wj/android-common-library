package com.wj.demo.ui.activity;

import android.content.Intent;

import com.wj.demo.R;
import com.wj.demo.ui.base.BaseActivity;
import com.wj.library.helper.UIHelper;
import com.wj.library.widget.GifView;

import butterknife.BindView;

/**
 * @version 1.0
 * 启动应用时的加载界面
 * @author idea_wj 2015-11-05
 */
public class SplashActivity extends BaseActivity {
    private static String TAG = SplashActivity.class.getName();

    @BindView(R.id.gif_loading)
    GifView gifLodaing;

    @Override
    protected int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    protected void initView() {
        super.initView();
        gifLodaing.setTimes(1);
        gifLodaing.setOnFinishListener(new GifView.FinishListener() {
            @Override
            public void finish() {
                UIHelper.getInstance().startActivityAndFinish(SplashActivity.this, new Intent(SplashActivity.this, MainActivity.class));
            }
        });
    }
}