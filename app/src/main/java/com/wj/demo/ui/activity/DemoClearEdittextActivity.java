package com.wj.demo.ui.activity;

import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.wj.demo.R;
import com.wj.demo.ui.base.BaseActivity;
import com.wj.library.helper.ToolbarHelper;
import com.wj.library.widget.ClearEditText;

import butterknife.BindView;

/**
 * 可清空的EditText
 * Created by wuj on 2016/6/26.
 * @version 1.0
 */
public class DemoClearEdittextActivity extends BaseActivity {
    private static String TAG = DemoClearEdittextActivity.class.getSimpleName();

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.cet_txt)
    ClearEditText cetTxt;

    @Override
    protected int getLayout() {
        return R.layout.activity_demo_clear_edittext;
    }

    @Override
    protected void initView() {
        tvTitle.setText("可清空的EditText");
        ToolbarHelper.getInstance().initToolbar(this,toolbar,R.mipmap.ic_back);
    }
}
