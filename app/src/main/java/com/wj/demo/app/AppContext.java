package com.wj.demo.app;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.wj.library.base.MyBaseApplication;
import com.wj.library.net.okhttp.OkHttpUtils;

/**
 * 上下文
 * Created by idea_wj on 2016/6/1.
 * @version 1.0
 */
public class AppContext extends MyBaseApplication {

    private static AppContext appContext;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

            MultiDex.install(this);
    }

    public static AppContext getInstance() {
        if (appContext == null)
            appContext = new AppContext();
        return appContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        init();
    }

    private void init() {
        OkHttpUtils.init(this);
    }
}