package com.wj.library.widget;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created on 2017/10/9.
 *
 * @author xinxiong
 */
public class MyRootView extends FrameLayout {
    public MyRootView(Context context) {
        this(context, null);
    }

    public MyRootView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyRootView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (mOnDispatchTouchEvent != null) {
            mOnDispatchTouchEvent.onDispatch();
        }
        return super.dispatchTouchEvent(ev);
    }

    private OnDispatchTouchEvent mOnDispatchTouchEvent;

    public OnDispatchTouchEvent getOnDispatchTouchEvent() {
        return mOnDispatchTouchEvent;
    }

    public void setOnDispatchTouchEvent(OnDispatchTouchEvent onDispatchTouchEvent) {
        mOnDispatchTouchEvent = onDispatchTouchEvent;
    }

    public interface OnDispatchTouchEvent {
        void onDispatch();
    }
}
