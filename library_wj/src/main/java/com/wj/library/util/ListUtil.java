package com.wj.library.util;

import java.util.List;

/**
 * Created on 2017/9/29.
 *
 * @author xinxiong
 */
public class ListUtil {

    public static boolean isEmpty(List list) {
        return list == null || list.isEmpty();
    }

}
