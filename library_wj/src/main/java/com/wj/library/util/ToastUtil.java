package com.wj.library.util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.wj.library.R;


public final class ToastUtil {


    public static final String TAG = ToastUtil.class.getSimpleName();
    private static int CONTENT_LENGTH_FOR_LONG_DURATION_TOAST = 15;//toast长度超过这个是使用long

    private static Context sContext;

    private static Resources sResources = null;

    private static Toast sToast = null;

    private static Handler sHandler = new Handler(Looper.getMainLooper());

    public static void init(Context context) {
        sContext = context;
        sResources = sContext.getResources();
    }

    public static void show(int textResId) {
        show(sResources.getText(textResId));
    }

    public static void show(int textResId, int textSize) {
        show(sResources.getText(textResId), textSize);
    }

    public static void show(CharSequence text) {
        show(text, 18, R.drawable.toast_util_bg);
    }

    public static void show(CharSequence text, int textSize) {
        show(text, textSize, R.drawable.toast_util_bg);
    }

    /**
     * @param text           要显示的文案
     * @param textSize       字体大小
     * @param backResourceId 背景图
     */
    public static void show(final CharSequence text, final float textSize, final @DrawableRes int backResourceId) {
        if (sToast != null) {
            sToast.cancel();
        }

        if (TextUtils.isEmpty(text)) {
            return;
        }

        sHandler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    final TextView messageTv = (TextView) LayoutInflater.from(sContext).inflate(R.layout.layout_toast, null, false);
                    messageTv.setText(text);
                    messageTv.setTextSize(textSize);
                    setBackgroundResource(messageTv, backResourceId);
                    sToast = new Toast(sContext);
                    sToast.setView(messageTv);
                    sToast.setGravity(Gravity.CENTER_VERTICAL, sToast.getXOffset(), sToast.getYOffset());
                    sToast.setDuration(text.length() < CONTENT_LENGTH_FOR_LONG_DURATION_TOAST ?
                            Toast.LENGTH_SHORT : Toast.LENGTH_LONG);
                    sToast.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }

    public static void setBackgroundResource(@NonNull View view, @DrawableRes int resId) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            int paddingTop = view.getPaddingTop();
            int paddingLeft = view.getPaddingLeft();
            int paddingRight = view.getPaddingRight();
            int paddingBottom = view.getPaddingBottom();
            view.setBackgroundResource(resId);
            view.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
        } else {
            view.setBackgroundResource(resId);
        }
    }




    /**
     * 短，资源文件
     * @param context
     * @param msg
     */
    public static void toastShort(Context context, int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 短，自定义字符串
     * @param context
     * @param msg
     */
    public static void toastShort(Context context,String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * 长，资源文件
     * @param context
     * @param msg
     */
    public static void toastLong(Context context, int msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * 长，自定义字符串
     * @param context
     * @param msg
     */
    public static void toastLong(Context context,String msg){
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }
}
