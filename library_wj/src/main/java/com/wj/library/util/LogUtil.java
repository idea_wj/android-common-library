package com.wj.library.util;

/**
 * Created by idea_wj on 16/6/29.
 */

import android.util.Log;

/**
 * Log统一管理类
 */
public class LogUtil {

    private LogUtil() {

        throw new UnsupportedOperationException("cannot be instantiated"); //提示 不可以被实例化
    }

    public static boolean isDebug = true;// 是否需要打印bug，可以在application的onCreate函数里面初始化
    private static final String TAG = "LogUtil";

    public static void currentThread(String tag) {
        if (isDebug)
            Log.e(tag, " | Error | " + "Thread:" + Thread.currentThread().getId());
    }

    // 下面是传入自定义tag的函数
    public static void i(String tag, String msg) {
        if (isDebug)
            Log.i(tag, msg);
    }

    public static void d(String tag, String msg) {
        if (isDebug)
            Log.d(tag, msg);
    }

    /**
     * @param throwable
     */
    public static void e(String tag,Throwable throwable) {
        if (isDebug)
            Log.e(tag, " | Error | ", throwable);
    }

    public static void e(String tag, String msg) {
        if (isDebug) {
            if(msg.length() > 4000) {
                for(int i=0;i<msg.length();++i){
                    if(i+4000<msg.length()) {
                        Log.e(tag + "_" + i, msg.substring(i, i + 4000));
                        i=i+4000;
                    } else {
                        Log.e(tag + "_" + i, msg.substring(i, msg.length()));
                        i=i+msg.length();
                    }
                }
            } else
                Log.e(tag,msg);
        }
    }

    public static void v(String tag, String msg) {
        if (isDebug)
            Log.v(tag, msg);
    }

}