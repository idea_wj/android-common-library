package com.wj.library.base;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;

import java.util.Calendar;

import com.wj.library.R;
import com.wj.library.util.KeyBoardUtil;
import com.wj.library.widget.MyRootView;

import butterknife.ButterKnife;

/**
 * @author idea_wj 2015-01-18
 *         所有activity的基类
 * @version 1.0
 */
public abstract class MyBaseActivity extends AppCompatActivity implements View.OnClickListener {
    public static final int  MIN_CLICK_DELAY_TIME = 500;
    private             long lastClickTime        = 0;
    public MyBaseActivity mBaseActivity;
    public MyRootView     mRootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBaseActivity = this;
        View view = LayoutInflater.from(mBaseActivity).inflate(R.layout.layout_base, null, false);
        setContentView(view);

        mRootView = (MyRootView) view;
        View contentView = LayoutInflater.from(mBaseActivity).inflate(getLayout(), mRootView, false);
        mRootView.addView(contentView);
        ButterKnife.bind(this, contentView);
        getIntentData();
        initData();
        initView();
    }

    @Override
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            myOnClick(v);//若大于延时,则执行业务
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        KeyBoardUtil.closeKeybord(getCurrentFocus(), this);
        setContentView(R.layout.view_null);
    }

    protected abstract int getLayout();

    protected abstract void getIntentData();

    protected abstract void initView();

    protected abstract void initData();

    protected abstract void myOnClick(View view);
}