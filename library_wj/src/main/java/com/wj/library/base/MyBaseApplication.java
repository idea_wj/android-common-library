package com.wj.library.base;

import android.app.Application;

import com.wj.library.util.DeviceUtil;
import com.wj.library.util.ScreenUtil;

/**
 * 通用上下文
 * @author idea_wj 2016-06-01
 * @version 1.0
 */
public class MyBaseApplication extends Application {
    private static final String TAG = MyBaseApplication.class.getName();

    public static int DISPLAY_HEIGHT;// 屏幕高度
    public static int DISPLAY_WIDTH;// 屏幕宽度
    public static int STATUS_BAR_HEIGHT;// 状态栏高度
    public static int NAVIGATION_BAR_HEIGHT;// 底部栏返回高度



    private static MyBaseApplication myBaseApplication;


    @Override
    public void onCreate() {
        super.onCreate();
        myBaseApplication = this;
        initDisplay();
    }

    public static MyBaseApplication getInstance(){
        return myBaseApplication;
    }


    // 初始化屏幕宽高信息
    protected void initDisplay() {
        // 获得屏幕高度（像素）
        MyBaseApplication.DISPLAY_HEIGHT = getResources().getDisplayMetrics().heightPixels;
        // 获得屏幕宽度（像素）
        MyBaseApplication.DISPLAY_WIDTH = getResources().getDisplayMetrics().widthPixels;
        // 获得系统状态栏高度（像素）
        MyBaseApplication.STATUS_BAR_HEIGHT = ScreenUtil.getStatusBarHeight(getApplicationContext());
        //底部栏返回高度
        MyBaseApplication.NAVIGATION_BAR_HEIGHT = ScreenUtil.getNavigationBarOffset(getApplicationContext());
    }
    /**
     * 判断当前版本是否兼容目标版本的方法
     *
     * @param VersionCode
     * @return
     */
    public static boolean isMethodsCompat(int VersionCode) {
        int currentVersion = android.os.Build.VERSION.SDK_INT;
        return currentVersion >= VersionCode;
    }
}
