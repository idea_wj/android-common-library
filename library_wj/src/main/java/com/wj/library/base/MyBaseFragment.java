package com.wj.library.base;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;
import com.wj.library.R;
import com.wj.library.util.KeyBoardUtil;

import butterknife.ButterKnife;

/**
 * @author idea_wj 2015-08-05
 * @version 1.0
 * 所有Fragment的基类，所有定义的Fragment都需要继承它，统一管理
 */
public abstract class MyBaseFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = "MyBaseFragment";

    public static final int MIN_CLICK_DELAY_TIME = 500;
    private long lastClickTime = 0;



    @Override
    public void onClick(View v) {
        long currentTime = Calendar.getInstance().getTimeInMillis();
        if (currentTime - lastClickTime > MIN_CLICK_DELAY_TIME) {
            lastClickTime = currentTime;
            myOnClick(v);//若大于延时,则执行业务
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initData();
        initView(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        KeyBoardUtil.closeKeybord(getActivity().getCurrentFocus(),getActivity());
        getActivity().setContentView(R.layout.view_null);
    }

    protected abstract void initView(View view, Bundle savedInstanceState);
    protected abstract void initData();
    protected abstract void myOnClick(View view);
    protected abstract int getLayoutId();
}
