package com.wj.library.net.okhttp;


import com.wj.library.net.okhttp.https.HttpParams;
import com.wj.library.net.okhttp.request.OkHttpBaseRequestBuilder;

public interface Paramsable<T extends OkHttpBaseRequestBuilder> {
    T params(HttpParams httpParams);

    T addParams(String key, String value);
}
