package com.wj.library.net.okhttp.callback;

import android.text.TextUtils;

import com.wj.library.R;
import com.wj.library.net.okhttp.Response.BaseResponse;
import com.wj.library.util.GsonUtils;
import com.wj.library.util.LogUtil;
import com.wj.library.util.ToastUtil;


import java.io.IOException;

import okhttp3.Call;
import okhttp3.Response;

public abstract class JsonCallback<C extends BaseResponse> extends BaseCallback<C> {
    private final String TAG = this.getClass().getName();
    private Class<C> classType;


    public JsonCallback(Class<C> classType) {
        this.classType = classType;
    }


    @Override
    public void preFailure(Call call, Response response, Exception exception) {
        LogUtil.e(TAG,exception);
        onFailure(call, response, exception);
    }

    @Override
    public void preSuccess(C response) {
        onSuccess(response);
    }

    @Override
    public void onSuccessByOtherStatus(C response) {
        if (!TextUtils.isEmpty(response.getMessage())) {
            ToastUtil.show(response.getMessage());
        }
    }

    @Override
    public void onSuccessByOtherStatus(Response response, C myResponse) {
        onSuccessByOtherStatus(myResponse);
    }

    @Override
    public void sendFailureMessage(Call call, Response response, Exception exception) {
        super.sendFailureMessage(call, response, new IOException("Unexpected code " + response));
        if (response != null) {
            if (response.code() == 401) {
                //无操作,登录的接口已经做了相应的操作
            } else if (response.code() == 403) {
                sendToastMessage("无访问权限");
            } else {
                sendToastMessage(R.string.common_request_err_please_try_later);
            }
        } else {
            //网络超时或者网络不好的情况
        }
        sendFinishMessage();
    }

    @Override
    public C parseNetworkResponse(Response response) throws Exception {
        C gsonResponse = null;
        if (response.body() != null && response.body().charStream() != null) {
            gsonResponse = GsonUtils.getObject(response.body().string(), classType);
            if (gsonResponse != null) {
                if (gsonResponse.getStatus() != 0) {
                    //新的接口,不会等于0
                    switch (gsonResponse.getStatus()) {
                        case 1:
                            sendSuccessMessage(gsonResponse);
                            break;
                        default:
                            sendSuccessByOtherStatus(response, gsonResponse);
                            break;
                    }
                }
            }
        }
        return gsonResponse;
    }
}
