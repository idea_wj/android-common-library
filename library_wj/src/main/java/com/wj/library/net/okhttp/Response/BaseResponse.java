package com.wj.library.net.okhttp.Response;

public class BaseResponse<T> {

    public int    status;      // 1-表示成功；非1-表示失败
    public String message;
    public T      data;              //返回的数据


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
